import axios from "axios";

export const getRequestURL = (model) => model;

export function request(url, options) {
  return axios.request({
    url,
    timeout: 10000,
    ...options,
  });
}
