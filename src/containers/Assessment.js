import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Grid
} from '@material-ui/core';
import Title from '../components/Title';

// Generate Order Data
function createData(id, date, name, shipTo, paymentMethod, amount) {
  return { id, date, name, shipTo, paymentMethod, amount };
}

const rows = [
  createData(0, '20 Jun, 2021', 'Audia Damayanti', '10', 'Kesenian', 97.6),
  createData(1, '20 Jun, 2019', 'Mario', '10', 'Kesenian', 80),
  createData(2, '20 Jun, 2019', 'Rizky', '9', 'Fisika', 95),
  createData(3, '20 Jun, 2019', 'Dede Darmawan', '9', 'Fisika', 90),
  createData(4, '20 Jun, 2019', 'Aji', '9', 'Fisika', 85),
];

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

export default function Assessment() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Title>Daftar Nilai</Title>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Tanggal</TableCell>
                <TableCell>Nama</TableCell>
                <TableCell>Kelas</TableCell>
                <TableCell>Pelajaran</TableCell>
                <TableCell align="right">Nilai</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.id}>
                  <TableCell>{row.date}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.shipTo}</TableCell>
                  <TableCell>{row.paymentMethod}</TableCell>
                  <TableCell align="right">{row.amount}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div className={classes.seeMore}>
            <Link color="primary" href="/">
              Kembali ke dashboard
            </Link>
          </div>
        </Paper>
      </Grid>
    </React.Fragment>
  );
}