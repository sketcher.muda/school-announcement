import React from "react";
import {
  Switch,
  BrowserRouter as Router,
} from "react-router-dom";
import Layout from "./containers/Layout";

export function App() {
  return (
    <Router>
      <Switch>
        <Layout />
      </Switch>
    </Router>
  );
}

export default App;